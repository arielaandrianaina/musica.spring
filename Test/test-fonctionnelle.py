import logging
import sys
import os
import json
import http.client
import time
#to get the current working directory
logging.basicConfig(level=logging.DEBUG)
directory = os.getcwd()
STORE_DATA = {}
SLEEP_AFTER_DOCKER_START = 10
logging.info('The current directory is here now : {0}'.format(directory))

def verify_file(files: list):
    #verify if the docker compose file exist 
    #verify if file start / or add directory
    for i,file in enumerate(files):
        if(file.startswith('/') and os.path.exists(file)):
            continue
        elif (not file.startswith('/') and os.path.exists(directory + "/" + file)):
            files[i]=directory + "/" + file
        else:
            logging.error("The {0} are not present or not exist".format(file))
            sys.exit(1)

def start_docker_compose(file_docker):
    command="docker-compose -f {0} up -d".format(file_docker)
    os.system(command)
    time.sleep(SLEEP_AFTER_DOCKER_START)

def stop_docker_compose(file_docker):
    command="docker-compose -f {0} down".format(file_docker)
    os.system(command)

def verify_response(response: http.client.HTTPResponse,stores,responseHTTP): 
    header=response.getheaders()
    bodyStr=response.read().decode("utf-8")
    status=response.status
    if not bodyStr :
        body= {}
    else :
        body=json.loads(bodyStr)
    for store in stores:
        if store in body :
            STORE_DATA[store]=body[store]
    logging.info("The header containt : {0}".format(header))
    logging.info("The body containt : {0}".format(body))
    if "status" in responseHTTP:
        if responseHTTP["status"] == status:
            logging.info("The status code is : {0}".format(status))
        else :
            logging.error("The status is not {0} but reponse give : {1}".format(responseHTTP["status"],status))
    else:
        logging.warning("Verify Status code not found")
    if "headers" in responseHTTP:
        if "Content-Type" in responseHTTP["headers"]:
            if responseHTTP["headers"]["Content-Type"].casefold() == response.getheader('Content-Type').casefold() :
                logging.info("The Content-Type is correct : {0}".format(responseHTTP["headers"]["Content-Type"]))
            else:
                logging.error("The Content-Type are not correct : {0}".format(responseHTTP["headers"]["Content-Type"]))
    else:
        logging.warning("Verify header not found")

def replace_uri(uri: str):
    for key in STORE_DATA:
        env_key="${"+key+"}"
        if env_key in uri:
            val=str(STORE_DATA[key])
            uri=uri.replace(env_key,val)
    return uri

def start_http_client(file_data):
    # Opening JSON file
    fileJson = open(file_data)
    dataJson = json.load(fileJson)
    # Iterating through the json
    headers = {'Content-type': 'application/json'}
    for data in dataJson:
        host=data['host']
        port=data['port']
        uri=data['uri']
        method=data['method']
        dataHTTP=json.dumps(data['data'])
        store=data['store']
        responseHTTP=data['response']
        try:
            connection=http.client.HTTPConnection(host,port)
            uri=replace_uri(uri)
            logging.info("THE URL : http://{0}:{1}{2} AND METHODE {3} ".format(host,port,uri,method))
            if(method=="GET"):
                connection.request(method,uri)
            elif(method=="POST" or method=="PUT" or method=="DELETE"):
                connection.request(method,uri,dataHTTP,headers=headers)
            response=connection.getresponse()
            verify_response(response,store,responseHTTP)
            connection.close()
        except ConnectionRefusedError as e:
            logging.error(e)
def main():
    files=[]
    if(len(sys.argv)==3) :
        if(sys.argv[1]=="-data" and ".json" in sys.argv[2]) :
            files.append(sys.argv[2])
        else:
            logging.error("Read documentation : need a json file")
            exit(1)
    elif(len(sys.argv)==5):
        if(sys.argv[1]=="-docker" and sys.argv[3]=="-data" and ".json" in sys.argv[4]) :
            files.append(sys.argv[2])
            files.append(sys.argv[4])
        else:
            logging.error("Read documentation : need a docker-compose file and json file")
            exit(1)
    else:
        logging.error("Read documentation : no parameter found")
        exit(1)
    verify_file(files)
    if(len(files)==1):
        dataJsonFile=files[0]
        logging.info("The data file exist : {0}".format(dataJsonFile))
        start_http_client(dataJsonFile)
    elif(len(files)==2):
        dockerFile=files[0]
        dataJsonFile=files[1]
        logging.info ("The docker compose exist : {0}".format(dockerFile))
        logging.info("The data file exist : {0}".format(dataJsonFile))
        start_docker_compose(dockerFile)
        start_http_client(dataJsonFile)
        stop_docker_compose(dockerFile)
    else:
        logging.error("The API detect a bug , report the bug please")
        
if __name__ == "__main__":
    main()