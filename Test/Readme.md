# Documentation for Web API TEST V1 with python :
The Web API Test need files , so for use . <br>
**Example** <br>
You can use with docker-compose file and data.json 
```
python3 test-fonctionnelle.py -docker docker-compose.yaml -data data.json 
```
You can use with data.json only 
```
python3 test-fonctionnelle.py -data data.json 
```
# Data for test 
For use this API you need file json with all configuration like that :
1. The file need to be json file with extension ".json" and array json in file
2. The structure of json <br>
**Example** <br>
```
{
    "host": "localhost",
    "port": "8085",
    "uri": "/playlist/MADA",
    "method": "POST",
    "data": {
        "name": "MADA261"
    },
    "store": [
        "id"
    ],
    "response": {
        "status": 200,
        "headers": {
            "Content-Type": "application/json"
        }
    }
}
```
* host : the host of web api
* port : the port of web api
* uri : the uri of web api 
    * You can write uri simply , don't forget '/' to start URI
    * You can write uri with store data like : <br>
    ``` /playlist/MADA/${id}' ``` <br>
    It'll change all '${..}' by the data store if you store
* method : The HTTP method , it takes onlys GET,POST,PUT,DELETE
* data : The data to send  and here type is 'application/json'
* store : all data on response can store and re-use if you want in URI but you need to precise the key of data 
* response : 
    * status : verify if the status code is correctly with the reponse give but it's optionnal
    * headers : 
        * "Content-Type" : verify if the Content-Type is correctly with the reponse give but it's optionnal
