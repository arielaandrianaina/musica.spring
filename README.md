# MUSICA : 
Une application en architecture microservice pour une bibliotheque de musiques
***
## MUSICA SPRING BOOT
- Avec une base de donné MySQl 
- Le service va contenir les playlist et les favoris des utilisateurs
## TEST
- Test d'integration avez <b>Spring boot test</b>
- Utilisation d'une base de donné en mémoire avec <b> H2 Memory Database </b>
- Le dossier est dans : <b>src/test</b>
    - le dossier <b>ressources</b> contient les configurations lors des test 
- Le dossier <b>Test</b> contient la version Web API Test V1
## Deploiement
- Docker :
    - Dockerfile
- Kubernetes :
    - deploy.yml
    - service.yml

# URI
<table>
    <thead>
        <tr>
            <th>Description</th>
            <th>METHODE</th>
            <th>URI</th>
            <th>BODY</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Creer une playlist </td>
            <td>POST</td>
            <td>playlist/{user}</td>
            <td>
            {
                "name":"MADA261"
            }
            </td>
        <tr>
        <tr>
            <td>Recupérer les playlist </td>
            <td>GET </td>
            <td>playlist/{user}</td>
            <td>
            </td>
        <tr>
        <tr>
            <td>Recupérer une playlist </td>
            <td>GET </td>
            <td>playlist/{user}/{playlist}</td>
            <td>
            </td>
        <tr>
        <tr>
            <td>Mettre à jour une playlist </td>
            <td>PUT</td>
            <td>playlist/{user}/{playlist}</td>
            <td>
            {
                "name":"MADA261"
            }
            </td>
        <tr>
        <tr>
            <td>Supprimer une playlist </td>
            <td>DELETE</td>
            <td>playlist/{user}/{playlist}</td>
            <td></td>
        </tr>
        <tr>
            <td>Ajouter des musics dans une playlist </td>
            <td>PUT </td>
            <td>playlist/music/{user}/{playlist}</td>
            <td>
            {
                [
                    "....",
                    ...
                ]
            }
            </td>
        <tr>
        <tr>
            <td>Supprimer les musics d'une playlist</td>
            <td>DELETE </td>
            <td>playlist/music/{user}/{playlist}</td>
            <td>
            {
                [
                    "....",
                    ...
                ]
            }
            </td>
        <tr>
        <tr>
            <td>Ajouter un favoris</td>
            <td>POST </td>
            <td>favoris/{user}/{musicid}</td>
            <td>
            </td>
        <tr>
        <tr>
            <td>Supprimer un favoris</td>
            <td>DELETE </td>
            <td>favoris/{user}/{musicid}</td>
            <td>
            </td>
        <tr>
        <tr>
            <td>Recuperer les favoris</td>
            <td>GET </td>
            <td>favoris/{user}</td>
            <td>
            </td>
        <tr>
    </tbody>
</table>