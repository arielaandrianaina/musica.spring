package com.musica.musica.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.musica.musica.models.Favoris;

public interface IFavorisRepository extends CrudRepository<Favoris, Integer> {
    Optional<Favoris> findByMusicIdAndUserId(String musicId,String userId);
    List<Favoris> findByUserId(String userId);
}
