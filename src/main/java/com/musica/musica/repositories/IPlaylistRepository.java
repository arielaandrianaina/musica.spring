package com.musica.musica.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.musica.musica.models.Playlist;

public interface IPlaylistRepository extends CrudRepository<Playlist, Integer> {
    List<Playlist> findByUserId(String userId);

    Optional<Playlist> findByUserIdAndPlaylistId(String userId, Integer playlistId);
}