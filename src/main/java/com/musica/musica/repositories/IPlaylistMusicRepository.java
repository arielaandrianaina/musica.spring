package com.musica.musica.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.musica.musica.models.Playlist;
import com.musica.musica.models.PlaylistMusic;

public interface IPlaylistMusicRepository extends CrudRepository<PlaylistMusic, Integer> {
    Optional<PlaylistMusic> findByPlaylistAndMusicId(Playlist playlist, String musicId);
}
