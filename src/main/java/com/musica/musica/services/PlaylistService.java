package com.musica.musica.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.musica.musica.dto.PlaylistDto;
import com.musica.musica.dto.PlaylistMusicDto;
import com.musica.musica.models.Playlist;
import com.musica.musica.models.PlaylistMusic;
import com.musica.musica.repositories.IPlaylistMusicRepository;
import com.musica.musica.repositories.IPlaylistRepository;

@Service
public class PlaylistService {
    @Autowired
    IPlaylistRepository repository;

    @Autowired
    IPlaylistMusicRepository repositoryPlaylistMusic;

    public Playlist create(String playlistName, String userId) {
        Playlist playlist = new Playlist();
        System.out.println(userId);
        playlist.setPlaylistName(playlistName);
        playlist.setUserId(userId);
        this.repository.save(playlist);
        return playlist;
    }

    public List<Playlist> findAll(String userId) {
        return this.repository.findByUserId(userId);
    }

    public Playlist find(String userId, int id) throws Exception {
        if (this.repository.findByUserIdAndPlaylistId(userId, id).isPresent())
            return this.repository.findByUserIdAndPlaylistId(userId, id).get();
        else
            throw new Exception("Playlist not found");
    }

    public Playlist update(String userId, int id, String name) throws Exception {
        if (this.repository.findByUserIdAndPlaylistId(userId, id).isPresent()) {
            var playlist = this.repository.findByUserIdAndPlaylistId(userId, id).get();
            playlist.setPlaylistName(name);
            this.repository.save(playlist);
            return playlist;
        } else
            throw new Exception("Playlist not found");
    }

    public void delete(String userId, Integer playlistId) throws Exception {
        if (this.repository.findByUserIdAndPlaylistId(userId, playlistId).isPresent()) {
            var playlist = this.repository.findByUserIdAndPlaylistId(userId, playlistId).get();
            var playlistMusics = playlist.getMusics();
            this.repositoryPlaylistMusic.deleteAll(playlistMusics);
            this.repository.delete(playlist);
        } else
            throw new Exception("Playlist not found");
    }

    public void updateMusic(String userId, int id, List<String> musics) throws Exception {
        List<PlaylistMusic> playlistMusics = new ArrayList<PlaylistMusic>();
        Playlist playlist = this.repository.findByUserIdAndPlaylistId(userId, id).get();
        for (String music : musics) {
            PlaylistMusic playlistMusic = new PlaylistMusic();
            playlistMusic.setMusicId(music);
            playlistMusic.setPlaylist(playlist);
            playlistMusics.add(playlistMusic);
        }
        this.repositoryPlaylistMusic.saveAll(playlistMusics);
    }

    public void deleteMusic(String userId, int id, List<String> musics) throws Exception {
        Playlist playlist = this.repository.findByUserIdAndPlaylistId(userId, id).get();
        for (String music : musics) {
            var del = this.repositoryPlaylistMusic.findByPlaylistAndMusicId(playlist, music).get();
            this.repositoryPlaylistMusic.delete(del);
        }
    }

    public PlaylistMusicDto Get(Playlist list) throws Exception {
        PlaylistMusicDto response = new PlaylistMusicDto();
        response.setId(list.getPlaylistId());
        response.setName(list.getPlaylistName());
        if (!list.getMusics().isEmpty()) {
            List<String> musics = new ArrayList<String>();
            for (PlaylistMusic element : list.getMusics()) {
                musics.add(element.getMusicId());
            }
            response.setMusicId(musics);
        }
        return response;
    }

    public List<PlaylistDto> GetList(List<Playlist> list) throws Exception {
        List<PlaylistDto> response = new ArrayList<PlaylistDto>();
        for (Playlist playlist : list) {
            PlaylistDto playlistDto = createDto(playlist);
            response.add(playlistDto);
        }
        return response;
    }

    public PlaylistDto createDto(Playlist playlist) {
        PlaylistDto playlistDto = new PlaylistDto();
        playlistDto.setId(playlist.getPlaylistId());
        playlistDto.setName(playlist.getPlaylistName());
        return playlistDto;
    }
}
