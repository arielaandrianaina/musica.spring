package com.musica.musica.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.musica.musica.dto.FavorisDto;
import com.musica.musica.models.Favoris;
import com.musica.musica.repositories.IFavorisRepository;

@Service
public class FavorisService {
    @Autowired
    IFavorisRepository repository;
    
    public Favoris create(String musicId, String userId) {
        Favoris favoris = new Favoris();
        System.out.println(userId);
        favoris.setMusicId(musicId);
        favoris.setUserId(userId);
        this.repository.save(favoris);
        return favoris;
    }

    public List<Favoris> findAll(String userId) {
        return this.repository.findByUserId(userId);
    }

    public void delete(String userId, String musicId) throws Exception {
        if (this.repository.findByMusicIdAndUserId(musicId, userId).isPresent()) {
            var favoris = this.repository.findByMusicIdAndUserId(musicId, userId).get();
            this.repository.delete(favoris);
        } else
            throw new Exception("Favori not found");
    }

    public FavorisDto createDto(List<Favoris> favoris) {
        FavorisDto favorisDto = new FavorisDto();
        List<String> list = new ArrayList<>();
        for (Favoris favori : favoris) {
            list.add(favori.getMusicId());
        }
        favorisDto.setMusic(list);
        return favorisDto;
    }


}
