package com.musica.musica.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.musica.musica.dto.PlaylistDto;
import com.musica.musica.dto.PlaylistMusicDto;
import com.musica.musica.models.Playlist;
import com.musica.musica.services.PlaylistService;

@RestController
@RequestMapping("playlist")
public class PlaylistController {
    @Autowired
    PlaylistService _service;

    // Add new playlist
    // playlist/user + body : { name }
    @RequestMapping(value = "{user}", method = RequestMethod.POST)
    public ResponseEntity<PlaylistDto> Create(@PathVariable String user,
            @RequestBody PlaylistDto dto) {
        Playlist playlist = this._service.create(dto.getName(), user);
        PlaylistDto response = this._service.createDto(playlist);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    // Get playlist according to user
    // playlist/user
    @RequestMapping(value = "{user}", method = RequestMethod.GET)
    public ResponseEntity<List<PlaylistDto>> GetAll(@PathVariable String user) throws Exception {
        List<Playlist> list = this._service.findAll(user);
        List<PlaylistDto> response = this._service.GetList(list);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    // Search a playlistId according to user specified
    // playlist/user/playlist
    @RequestMapping(value = "{user}/{playlist}", method = RequestMethod.GET)
    public ResponseEntity<PlaylistMusicDto> Get(@PathVariable String user,
            @PathVariable Integer playlist) throws Exception {
        Playlist list = this._service.find(user, playlist);
        PlaylistMusicDto response = this._service.Get(list);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    // Update playlist
    // playlist/user/playlist
    @RequestMapping(value = "{user}/{playlist}", method = RequestMethod.PUT)
    public ResponseEntity<PlaylistDto> Update(@PathVariable String user,
            @PathVariable Integer playlist, @RequestBody PlaylistDto dto) throws Exception {
        Playlist playlistUpdated = this._service.update(user, playlist, dto.getName());
        PlaylistDto response = this._service.createDto(playlistUpdated);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    
    //supprime une playlist spécifique associée à un utilisateur
    // playlist/user/playlist
    @RequestMapping(value = "{user}/{playlist}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> Delete(@PathVariable String user, @PathVariable Integer playlist)
            throws Exception {
        this._service.delete(user, playlist);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    // Update music in playlist
    // playlist/user/playlist
    @RequestMapping(value = "music/{user}/{playlist}", method = RequestMethod.PUT)
    public ResponseEntity<Object> UpdateMusic(@PathVariable String user,
            @PathVariable Integer playlist, @RequestBody List<String> musicsId) throws Exception {
        this._service.updateMusic(user, playlist, musicsId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    // Delete music playlist
    // playlist/user/playlist
    @RequestMapping(value = "music/{user}/{playlist}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> DeleteMusic(@PathVariable String user,
            @PathVariable Integer playlist, @RequestBody List<String> musicsId) throws Exception {
        this._service.deleteMusic(user, playlist, musicsId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}