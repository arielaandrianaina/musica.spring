package com.musica.musica.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.musica.musica.dto.FavorisDto;
import com.musica.musica.models.Favoris;
import com.musica.musica.services.FavorisService;

@RestController
@RequestMapping("favoris")
public class FavorisController {
    @Autowired
    FavorisService _service;

    // Add new Favoris
    // Favoris/user/music 
    @RequestMapping(value = "{user}/{music}", method = RequestMethod.POST)
    public ResponseEntity<Object> Create(@PathVariable String user, @PathVariable String music) {
        _service.create(music, user);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    // Delete music Favoris
    // Favoris/user/FavorisId
    @RequestMapping(value = "{user}/{music}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> delete(@PathVariable String user, @PathVariable String music) throws Exception {
        this._service.delete(user,  music);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    // Get Favoris according to user
    // Favoris/user
    @RequestMapping(value = "{user}", method = RequestMethod.GET)
    public ResponseEntity<FavorisDto> findAll(@PathVariable String user) throws Exception {
        List<Favoris> list = this._service.findAll(user);
        FavorisDto response = this._service.createDto(list);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    
}
    
    