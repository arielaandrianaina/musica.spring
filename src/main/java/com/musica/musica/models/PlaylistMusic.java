package com.musica.musica.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;

@Entity
public class PlaylistMusic {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer playlistMusicId;
    private String musicId;
    @ManyToOne
    @JoinColumn(name = "playlistId")
    private Playlist playlist;

    public String getMusicId() {
        return musicId;
    }

    public void setMusicId(String musicId) {
        this.musicId = musicId;
    }

    public Playlist getPlaylist() {
        return playlist;
    }

    public void setPlaylist(Playlist playlist) {
        this.playlist = playlist;
    }

    public Integer getPlaylistMusicId() {
        return playlistMusicId;
    }

    public void setPlaylistMusicId(Integer playlistMusicId) {
        this.playlistMusicId = playlistMusicId;
    }
}
