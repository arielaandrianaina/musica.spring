package com.musica.musica.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Favoris {
    @Id 
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer favorisId;
    @Column(length = 255)
    private String musicId;
    @Column(length = 255)
    private String userId;
	public Integer getFavorisId() {
		return favorisId;
	}
	public void setFavorisId(Integer favorisId) {
		this.favorisId = favorisId;
	}
	public String getMusicId() {
		return musicId;
	}
	public void setMusicId(String musicId) {
		this.musicId = musicId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
    
}
