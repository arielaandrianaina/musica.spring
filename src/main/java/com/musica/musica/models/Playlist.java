package com.musica.musica.models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity
public class Playlist {
    @Id 
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer playlistId;
    @Column(length = 255)
    private String userId;
    private String playlistName;
    @OneToMany
    @JoinColumn(name = "playlistId", nullable = true)
    private List<PlaylistMusic> musics;

    public String getPlaylistName() {
        return playlistName;
    }

    public void setPlaylistName(String playlistName) {
        this.playlistName = playlistName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<PlaylistMusic> getMusics() {
        return musics;
    }

    public void setMusics(List<PlaylistMusic> musics) {
        this.musics = musics;
    }

    public Integer getPlaylistId() {
        return playlistId;
    }

    public void setPlaylistId(Integer playlistId) {
        this.playlistId = playlistId;
    }

}
