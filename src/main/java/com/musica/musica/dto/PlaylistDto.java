package com.musica.musica.dto;

import org.springframework.lang.Nullable;

public class PlaylistDto {
    @Nullable
    private Integer id;

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
