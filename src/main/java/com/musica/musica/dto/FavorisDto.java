package com.musica.musica.dto;

import java.util.List;

public class FavorisDto {
    private List<String> music;

	public List<String> getMusic() {
		return music;
	}

	public void setMusic(List<String> music) {
		this.music = music;
	}
    
}
