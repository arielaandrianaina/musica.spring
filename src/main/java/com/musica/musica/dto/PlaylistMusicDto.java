package com.musica.musica.dto;

import java.util.List;

public class PlaylistMusicDto {
    private Integer id;
    private List<String> musicId;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<String> getMusicId() {
        return musicId;
    }

    public void setMusicId(List<String> musicId) {
        this.musicId = musicId;
    }
}
