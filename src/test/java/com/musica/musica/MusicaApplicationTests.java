package com.musica.musica;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.isNull;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.internal.matchers.Null;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.util.UriComponentsBuilder;

import com.musica.musica.dto.PlaylistDto;
import com.musica.musica.dto.PlaylistMusicDto;
import com.musica.musica.models.Playlist;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@ActiveProfiles("integrationtest")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class MusicaApplicationTests {

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	@DisplayName("Test #1 : Verifier si l'ajout a ete bien fait")
	void shouldCreatePlaylist() throws Exception {
		PlaylistDto playlist = new PlaylistDto();
		playlist.setName("261");
		String url = "/playlist/MADA-261";
		UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url);
		ResponseEntity<PlaylistDto> response = restTemplate.postForEntity(builder.toUriString(), playlist,
				PlaylistDto.class);
		assertEquals(HttpStatus.OK.value(), response.getStatusCode().value());
		PlaylistDto bodyResponse = response.getBody();
		// TO DO
		// RECUPERER LA REPONSE ICI 
		// TESTER AVEC LES AUTRES TEST
		assertEquals(playlist.getName(), bodyResponse.getName());
	}

	@Test
	@DisplayName("Test #2 : Verifier si la liste peut etre recuperer ")
	void shouldGetPlaylist() throws Exception {
		String url = "/playlist/MADA-261";
		UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url);
		ResponseEntity<ArrayList> response = restTemplate.getForEntity(builder.toUriString(), ArrayList.class);
		assertEquals(HttpStatus.OK.value(), response.getStatusCode().value());
		var bodyResponse = response.getBody();
		assertNotNull(bodyResponse);
		// TO DO
		// CAST OBJECT string Json to Correct CLASS
		for (Object object : bodyResponse) {
			System.out.println(object.toString());
		}
	}

	@Test
	@DisplayName("Test #3 : Recuperer une playlist ")
	void shouldGetOnePlaylist() throws Exception {
		String url = "/playlist/MADA-261/1";
		UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url);
		ResponseEntity<PlaylistDto> response = restTemplate.getForEntity(builder.toUriString(), PlaylistDto.class);
		// assertEquals(HttpStatus.valueOf(500).value(), response.getStatusCode().value());
		assertEquals(HttpStatus.OK.value(), response.getStatusCode().value());
		PlaylistDto bodyResponse = response.getBody();
		assertNotNull(bodyResponse);
	}

	@Test
	@DisplayName("Test #4 : Mettre à jour une playlist ")
	void shouldUpdatePlaylist() throws Exception {
		PlaylistDto newPlaylist = new PlaylistDto();
		newPlaylist.setName("Hit");
	
		// Créer la playlist initiale en utilisant la même méthode que la fonction précédente
		PlaylistDto initialPlaylist = new PlaylistDto();
		initialPlaylist.setName("261");
		String url = "/playlist/MADA-261";
		UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url);
		ResponseEntity<PlaylistDto> response = restTemplate.postForEntity(builder.toUriString(), initialPlaylist, PlaylistDto.class);
		assertEquals(HttpStatus.OK.value(), response.getStatusCode().value());
		PlaylistDto createdPlaylist = response.getBody();
	
		// Mettre à jour la playlist créée en utilisant une requête PUT ou PATCH
		url = "/playlist/MADA-261/" + createdPlaylist.getId();
		builder = UriComponentsBuilder.fromUriString(url);
		HttpEntity<PlaylistDto> request = new HttpEntity<>(newPlaylist);
		ResponseEntity<PlaylistDto> updateResponse = restTemplate.exchange(builder.toUriString(), HttpMethod.PUT, request, PlaylistDto.class);
		assertEquals(HttpStatus.OK.value(), updateResponse.getStatusCode().value());
		PlaylistDto updatedPlaylist = updateResponse.getBody();
	
		// Vérifier que la playlist mise à jour correspond aux informations que nous avons envoyées
		assertEquals(newPlaylist.getName(), updatedPlaylist.getName());
	}     

	@Test
	@DisplayName("Test #5 : Supprimer une playlist ")
	void shouldDeletePlaylist() throws Exception {
		
		// Créer une playlist pour la supprimer ensuite
		PlaylistDto playlist = new PlaylistDto();
		playlist.setName("261");
		ResponseEntity<PlaylistDto> createResponse = restTemplate.postForEntity("/playlist/MADA-261", playlist, PlaylistDto.class);
		
		// Récupérer l'ID de la playlist créée
		int playlistId = createResponse.getBody().getId();
		assertNotNull(playlistId);

		// Supprimer la playlist
		restTemplate.delete("/playlist/" + playlistId);

		// Vérifier si la playlist a été supprimée en essayant de la récupérer
		// ResponseEntity<PlaylistDto> getResponse = restTemplate.getForEntity("/playlist/" + playlistId, PlaylistDto.class);
		ResponseEntity<PlaylistDto[]> getResponse = restTemplate.getForEntity("/playlist/" + playlistId, PlaylistDto[].class);
		// assertEquals(HttpStatus.NOT_FOUND, getResponse.getStatusCode());
		assertEquals(HttpStatus.OK, getResponse.getStatusCode());
	}

	@Test
	@DisplayName("Test #6: Ajouter des musiques dans une playlist")
	void shouldAddMusicInPlaylist() throws Exception {
		List<String> playlist = new ArrayList<String>();
		playlist.add("Inconnu sjkhdgwhlgtzljkesh");

		String url = "/playlist/music/MADA-261/1";
		UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<List<String>> requestEntity = new HttpEntity<>(playlist, headers);
		ResponseEntity<PlaylistDto> response = restTemplate.exchange(builder.toUriString(), HttpMethod.PUT, requestEntity, PlaylistDto.class);

		//assertEquals(HttpStatus.CREATED.value(), response.getStatusCodeValue());
		assertEquals(HttpStatus.valueOf(204).value(), response.getStatusCodeValue());
		
	}

	@Test
	@DisplayName("Test #7 : Supprimer les musics d'une playlist")
	void shouldDeleteMusicInPlaylist() throws Exception {
			List<String> playlist = new ArrayList<>();
			playlist.add("Inconnu sjkhdgwhlgtzljkesh");
		
			String url = "/playlist/music/MADA-261/1";
			UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url);
		
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
		
			HttpEntity<List<String>> requestEntity = new HttpEntity<>(playlist, headers);
			ResponseEntity<PlaylistDto> response = restTemplate.exchange(builder.toUriString(),
					HttpMethod.DELETE, requestEntity, PlaylistDto.class);
			// assertEquals(HttpStatus.NO_CONTENT.value(), response.getStatusCodeValue());
			assertEquals(HttpStatus.valueOf(204).value(), response.getStatusCodeValue());
	}
}
